#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include "string.h"

using namespace std;

void deleteWords(char*, int);
void deleteArrayWords(char*, int[], int);
void getDeleteTests(char*);
void getUserMode(char*);
void getDeleteLine(char*, int[], int);

int main()
{
	const int size = 256;
	char source[size] = "Lorem lorem   Ipsum, is simply  79 dummy text of the"
		" printing - and typesetting : % simply. Lorem Ipsum has been"
		" the industry's , simply dummy text ever since the 1500s, simply";

	while (true) {
		int n;
		cout << "Press 1 to enable test mode" << endl;
		cout << "Press 2 to set your size" << endl;
		cout << "Press 3 to exit" << endl;
		cin >> n;
		system("cls");

		switch (n)
		{
		case 1:
			getDeleteTests(source);
			return 0;
			system("pause");
			break;
		case 2:
			getUserMode(source);
			return 0;
			break;
		case 3:
			return 0;
		default:
			break;
		}
	}

	return 0;
}

void deleteArrayWords(char* source, int sizes[], int counter) {
	for (int i = 0; i < counter; i++) {
		deleteWords(source, sizes[i]);
	}
};

void deleteWords(char* source, int count)
{
	const char* symbols = "ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";
	char* pword = strpbrk(source, symbols);

	while (pword) {
		int length = strspn(pword, symbols);

		if (length == count) {
			char* p = pword + length;
			strcpy(pword, p);
		}
		else {
			pword += length;
		}

		pword = strpbrk(pword, symbols);
	}

	//for (int i = 0; i < count; i++) {
	//	char* word = words[i];
	//	int length = strlen(word);
	//	char* pword = strstr(source, word);

	//	while (pword) {
	//		char* p = pword + length;

	//		strcpy(pword, p);
	//		pword = strstr(source, word);
	//	}
	//}
}

void getDeleteTests(char* source)
{
	const int size = 26;
	int count = 0;

	ifstream streamOut;
	streamOut.open("sizes.txt");

	if (!streamOut.is_open()) {
		cout << "Cannot open this file";
		system("pause");
	}

	int sizes[size];

	while (!streamOut.eof()) {
		streamOut >> sizes[count];
		count++;
	}

	getDeleteLine(source, sizes, count);
	streamOut.close();

}

void getUserMode(char* source)
{
	int count = 0, size = 26;
	cout << "Enter size of words: ";
	cin >> count;
	system("cls");

	//char** words = new char* [size];

	//for (int i = 0; i < count; i++) {
	//	char* word = new char[size];
	//	while (true) {
	//		cout << "Enter your word: " << endl;
	//		cin.getline(word, size);
	//		system("cls");

	//		if (word[0] != '\0') {
	//			break;
	//		}
	//	}
	//	words[i] = word;
	//}

	cout << "This is an original line: " << endl << source << endl << endl;
	cout << "This is line without words with size " << count;

	deleteWords(source, count);
	cout << endl << source;

}

void getDeleteLine(char* source, int sizes[], int count)
{
	cout << "This is an original line: " << endl << source << endl << endl;
	cout << "This is line without words with size ";

	for (int i = 0; i < count; i++)
	{
		if (i == count - 1) {
			cout << sizes[i] << ": ";
		}
		else {
			cout << sizes[i] << " ";
		}
	}

	deleteArrayWords(source, sizes, count);
	cout << endl << source;
}
